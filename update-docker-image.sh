#!/bin/bash -xe

LABEL=`git rev-parse --short HEAD`
if [[ $# -gt 0 ]]
then
    LABEL=$1
    echo "LABEL OVERRIDDEN: $LABEL"
else
    echo "Using label: ${LABEL}"
fi

docker build -t registry.gitlab.com/ilyaivantsov/bot-embassy:${LABEL} .
docker push registry.gitlab.com/ilyaivantsov/bot-embassy:${LABEL}
kubectl --kubeconfig="$HOME/.kube/k8s-stage-kubeconfig.yaml" -n obvious-choice set image deployment/bot-embassy bot-embassy=registry.gitlab.com/ilyaivantsov/bot-embassy:${LABEL}
