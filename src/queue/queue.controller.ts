import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { SqsService } from './sqs.service';

@Controller('queue')
export class QueueController {
    constructor(
        private readonly sqsService: SqsService,
    ) { }

    /**
     * 
     * @param name Name of FIFO queue
     * @returns {Promise<string>} URL of QUEUE
     */
    @Get('create')
    createSqs(
        @Query('name') name: string
    ): Promise<string> {
        return this.sqsService.createFIFOQueue(name);
    }

    @Post('send')
    async sendMessage(
        @Body('queueURL') queueURL: string,
        @Body('data') data: any
    ) {
        await this.sqsService.sendMessage(queueURL, data);
        return 'OK';
    }

    @Get('receive')
    receive(
        @Query('queueURL') queueURL: string
    ): Promise<any> {
        return this.sqsService.receiveMessage(queueURL);
    }

    @Get('info')
    info(@Query('queueURL') queueURL: string): Promise<string> {
        return this.sqsService.getInfo(queueURL);
    }

    @Get('clear')
    clear(@Query('queueURL') queueURL: string): Promise<void> {
        return this.sqsService.clearQueue(queueURL);
    }

    @Get('list')
    getListQueues(): Promise<string[]> {
        return this.sqsService.getListQueues();
    }
}