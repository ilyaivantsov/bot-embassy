import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { GoogleSpreadsheet } from 'google-spreadsheet';
import * as _ from 'lodash';
import { HeadRow } from './enum/gs.enums';
import { IDataSheet } from './interface/gs.interfaces';
import { ConfigService } from '@nestjs/config';
import { Client } from 'src/core/interfaces';
import { isUndefined } from 'src/util';

@Injectable()
export class GsService implements OnModuleInit {
    private readonly logger = new Logger(GsService.name);

    constructor(private configService: ConfigService) { }

    public table = new GoogleSpreadsheet(this.configService.get<string>('GOOGLE_SHEETS_ID'));
    private serviceAccount = {
        client_email: this.configService.get<string>('GOOGLE_SERVICE_ACCOUNT_EMAIL'),
        private_key: this.configService.get<string>('GOOGLE_PRIVATE_KEY'),
    };

    private PROXY: string[] = this.configService.get<string>('PROXY').split(',');
    private PROXY_USERNAME: string = this.configService.get<string>('PROXY_USERNAME');
    private PROXY_PASSWORD: string = this.configService.get<string>('PROXY_PASSWORD');

    async createNewTab(data: IDataSheet): Promise<void> {
        const sheet = await this.table.addSheet();
        await sheet.updateProperties({ title: data.title });
        await sheet.setHeaderRow(data.headerRow);
        await sheet.addRows(data.data);
    }

    async getClients(attempts: { updates: number; duration: number; sign: number }, title: string = 'МСК', batch: number = 1): Promise<Client[][]> {
        await this.table.loadInfo();
        const sh = this.table.sheetsByTitle[title];
        if (typeof sh == 'undefined')
            return [];
        let data = await sh.getRows();
        // let clients = this.prepareClients(data, attempts);
        return _.chunk(this.prepareClients(data, attempts, title), batch);
    }

    async getInfo(): Promise<string[]> {
        await this.table.loadInfo();
        return Object.keys(this.table.sheetsByTitle);
    }

    private prepareClients(data: Array<any>, attempts: { updates: number; duration: number; sign: number }, tab: string): Client[] {
        return data.filter(client => isUndefined(client[HeadRow.STATUS])).map(client => {
            let [d_after, m_after, y_after] = client[HeadRow.DATE_AFTER].split('.'),
                [d_before, m_before, y_before] = client[HeadRow.DATE_BEFORE].split('.');
            return {
                attempts,
                tab,
                login: client[HeadRow.LOGIN].trim(),
                password: client[HeadRow.PASSWORD].trim(),
                importance: +client[HeadRow.IMPORTANCE],
                // status: client[HeadRow.STATUS],
                members: !+client[HeadRow.PEOPLE] ? 1 : +client[HeadRow.PEOPLE],
                afterDate: new Date(y_after, m_after - 1, d_after).getTime(),
                beforeDate: new Date(y_before, m_before - 1, d_before).getTime(),
                town: 'QUICK',
                proxy: this.PROXY[Math.floor(Math.random() * this.PROXY.length)],
                credentials: { username: this.PROXY_USERNAME, password: this.PROXY_PASSWORD },
            }
        });
        // return clients;
    }

    onModuleInit() {
        this.table.useServiceAccountAuth(this.serviceAccount)
            .then(() => this.logger.log('Auth to GS OK'))
            .catch(err => this.logger.error(err));
    }
}
