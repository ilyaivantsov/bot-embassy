import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { Client } from 'src/core/interfaces';
import { GsService } from './gs.service';

@Controller('gs')
export class GsController {

    constructor(private readonly gsServices: GsService) { }

    @Post('getclients')
    getClients(
        @Body('attempts') attempts: any,
        @Body('title') title?: string,
        @Body('batch') batch?: number,
    ): Promise<Client[][]> {
        return this.gsServices.getClients(attempts, title, batch);
    }

    @Get('info')
    getInfo(): Promise<string[]> {
        return this.gsServices.getInfo();
    }
}
