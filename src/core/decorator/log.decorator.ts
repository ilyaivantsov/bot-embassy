export function log(
    target: Object,
    propertyName: string,
    propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
    const method = propertyDesciptor.value;

    propertyDesciptor.value = function (...args: any[]) {
        const client = args[1];
        console.log(`${Date.now()} ${propertyName}  ${client?.login}`);
        return method.apply(this, args);
    }
    return propertyDesciptor;
};