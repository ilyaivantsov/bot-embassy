import { setTimeout } from 'timers';
import moment from 'moment';
import { SchedulerRegistry } from '@nestjs/schedule';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable, Logger } from '@nestjs/common';

import { CronJob } from 'cron';
import { Listener } from 'eventemitter2';
import { LaunchOptions, Page } from 'puppeteer';

import { SqsService } from 'src/queue/sqs.service';

import { BrowserService } from './puppeteer/browser.service';
import { Client } from './interfaces';
import { Town, AuthStatus, PathName, URLs, CoreEvents } from './enum';
import { isArrayFull, isUndefined } from 'src/util';

@Injectable()
export class CoreService {
    // TODO: Придумать формат логов
    // TODO: Добавлять логи в ObjectStorage
    private readonly logger = new Logger(CoreService.name);
    private listeners: Map<string, Listener> = new Map();
    private timeout: Map<string, NodeJS.Timeout> = new Map();

    constructor(
        private browserService: BrowserService,
        private sqs: SqsService,
        private scheduler: SchedulerRegistry,
        private eventEmitter: EventEmitter2
    ) { }
    /**
     * 
     * @param queue 
     * @param startAt DD.MM.YYYY HH:mm
     */
    turnOn(queue: string, startAt?: string) {
        const job = this.factory(queue, 'search');
        const listener = this.eventEmitter.on(queue, job.bind(this), { objectify: true });
        this.listeners.set(queue, listener as Listener);

        isUndefined(startAt) ? job.call(this) : this.timeout.set(queue, setTimeout(job.bind(this), this.setDelay(startAt)));
    }

    turnOff(queue: string) {
        this.timeout.delete(queue);
        const listener = this.listeners.get(queue);
        if (this.listeners.delete(queue))
            listener.off();
    }

    addCronJob(name: string, mask: string, queue: string) {
        const job = this.factory(queue, 'cron');
        const cron = new CronJob(mask, job.bind(this));
        this.scheduler.addCronJob(name, cron);
        cron.start();
    }

    deleteCron(name: string) {
        this.scheduler.deleteCronJob(name);
    }

    get getCrons(): Array<string> {
        const jobs = this.scheduler.getCronJobs();
        return [...jobs.keys()];
    }

    get getListeners(): Array<string> {
        return [...this.listeners.keys()];
    }

    private setDelay(startAt: string): number {
        const delay = moment(startAt, 'DD.MM.YYYY HH:mm');
        return delay.isValid() ? delay.diff(moment()) : 0;
    }

    private factory(queue: string, type: 'search' | 'cron') {
        return async function () {
            const clients = await this.sqs.receiveMessage(queue);
            if (!isArrayFull(clients)) // TODO: Add auto turnon/of
                return;
            const jobs: Array<Promise<void>> = clients.map((client) => this.mainFlow(client, type));
            await Promise.all(jobs);
            type === 'search' ? this.eventEmitter.emit(queue) : 0;
        }
    }

    private async mainFlow(client: Client, type: 'search' | 'cron') {
        const launch: LaunchOptions = {
            "headless": true,
            "args": [
                "--no-sandbox",
                "--disable-setuid-sandbox"
            ]
        };
        if (!isUndefined(client.proxy))
            launch.args.push(`--proxy-server=${client.proxy}`);

        const browser = await this.browserService.getBrowserInstance(launch, client.login);
        try {
            const page = await this.browserService.getPageInstance(browser, client.credentials || null);
            await this.authorization(page, client);
            // Select rus lang - 
            await this.fillOutForm(page, client);
            // TODO: 48 Start at if cron 
            await this.checkTimetable(page, client, type);
            await this.signUp(page, client);
            await browser.close();
        }
        catch (err) {
            this.logger.error(err);
            await browser.close();
        }
    }

    private async authorization(page: Page, client: Client, attempts: number = 2): Promise<void> {
        await this.browserService.authorization(page, client.login, client.password);
        const { status } = await this.browserService.authorizationStatus(page);

        if (status === AuthStatus.SUCCESS_AUTH) {
            this.eventEmitter.emit(CoreEvents.AUTH_SECCESS, client);
            return;
        }
        if (status === AuthStatus.ERROR_AUTHENTICATION && attempts > 1) {
            return await this.authorization(page, client, attempts - 1);
        }
        if (status === AuthStatus.CHANGE_PASSWORD) {
            this.eventEmitter.emit(CoreEvents.CHANGE_PASSWORD, client);
            return;
        }

        const error = await page.evaluate(() => document.querySelector('.errorM3').textContent.replace(/^\s*/, '').replace(/\s*$/, ''));
        this.eventEmitter.emit(CoreEvents.ERROR_AUTH, client, error);
        throw new Error('AUTH ERROR UNKNOWN ERROR');
    }

    private async fillOutForm(page: Page, client: Client): Promise<void> {
        const type = client.town;
        if (type === Town.QUICK) {
            await page.goto(URLs.SCHEDULE_APPOINTMENT);
            return;
        }
        if (type === Town.MOSCOW) {
            await this.browserService.toFillOutFormsMsk(page);
            return;
        }
        if (type === Town.YEKATERINBURG) {
            await this.browserService.toFillOutFormsEkat(page);
            return;
        }
        if (type === Town.VLADIVOSTOK) {
            await this.browserService.toFillOutFormsVld(page);
            return;
        }

        this.eventEmitter.emit(CoreEvents.ERROR_FILL_FORM, client);
        throw new Error('ERROR_FILL_FORM UNKNOWN ERROR');
    }

    private async checkTimetable(page: Page, client: Client, type: 'search' | 'cron'): Promise<void> {
        const updates: number = client.attempts.updates;
        const duration: number = client.attempts.duration;
        let timeTable;
        for (let i = 0; i < updates; ++i) {
            timeTable = await this.browserService.checkTimetable(page, duration);
            this.logger.log(`${client.login} update table ${i}`);
            if (isArrayFull(timeTable.dates)) {
                this.eventEmitter.emit(CoreEvents.AVAILABLE_DATES, client, timeTable.dates, type);
                return;
            }
        }
    }

    private async signUp(page: Page, client: Client): Promise<void> {
        let flag = false;
        const attempts: number = client.attempts.sign;
        // await this.browserService.screenshot(page, client.login).catch(err => this.logger.error(err));
        for (let i = 0; i < attempts; ++i) {
            this.logger.log(`${client.login} try to sign up ${i}`);
            let { signed } = await this.browserService.toSignUp(page, { after: client.afterDate, before: client.beforeDate }, flag);

            if (!signed) {
                const element = await page.$('#dashboard > div > p');
                const value = await page.evaluate(el => el.textContent, element);
                this.eventEmitter.emit(CoreEvents.DATE_DOES_NOT_FIT, client, value);
                return;
            }

            let path = await this.browserService.getPathNameOfCurrentPage(page);
            if (path == PathName.APPOINTMENT_CONFIRMATION) {
                this.eventEmitter.emit(CoreEvents.SUCCESS_SIGNED_UP, client);
                return;
            }
        }
    }
}
