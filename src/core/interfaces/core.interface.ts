import { Page } from "puppeteer";
import { AuthStatus } from "../enum";

export interface ITimeTableStatus {
    page: Page,
    dates: Array<IDates>
}

export interface IAuthStatus {
    page: Page;
    status: AuthStatus;
}

export interface ISignUpStatus {
    page: Page;
    signed: boolean;
}

export interface IDates {
    date: number;
    index: number;
}

export interface ISuitableDates {
    after: number;
    before: number;
}