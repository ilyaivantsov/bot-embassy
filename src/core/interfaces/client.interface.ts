import { AuthOptions } from "puppeteer";

export interface Client {
    login: string;
    password: string;
    tab: string;
    importance: number;
    afterDate: number;
    beforeDate: number;
    members: number;
    town: string;
    credentials?: AuthOptions;
    proxy?: string;
    attempts: { sign: number, updates: number, duration: number };
}