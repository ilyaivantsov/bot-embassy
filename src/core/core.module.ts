import { Module } from '@nestjs/common';
import { QueueModule } from 'src/queue/queue.module';
import { GsModule } from 'src/gs/gs.module';

import { CoreService } from './core.service';
import { CaptchaService } from './captcha/captcha.service';
import { BrowserService } from './puppeteer/browser.service';
import { CoreController } from './core.controller';
import { EventsGateway } from './events.gateway';

@Module({
  imports: [QueueModule, GsModule],
  providers: [CoreService, BrowserService, CaptchaService, EventsGateway],
  controllers: [CoreController],
  exports: [CoreService]
})
export class CoreModule { }
