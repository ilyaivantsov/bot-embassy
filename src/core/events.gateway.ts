import { Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import {
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'socket.io';
import * as _ from 'lodash';

import { SqsService } from '../queue/sqs.service';
import { GsService } from '../gs/gs.service';

import { CoreService } from './core.service';
import { CoreEvents } from './enum';
import { Client, IDates } from './interfaces';

@WebSocketGateway()
export class EventsGateway {
    constructor(
        private sqs: SqsService,
        private gs: GsService,
        private coreService: CoreService
    ) { }

    private readonly logger = new Logger(EventsGateway.name);

    @WebSocketServer()
    server: Server;

    @OnEvent(CoreEvents.AUTH_SECCESS)
    handleAUTH_SECCESS(client: Client) {
        this.logger.log(`${client.login} AUTH_SECCESS`);
        this.server.emit(CoreEvents.AUTH_SECCESS, client.login);
    }

    @OnEvent(CoreEvents.CHANGE_PASSWORD)
    handleCHANGE_PASSWORD(client: Client) {
        this.logger.warn(`${client.login} CHANGE_PASSWORD`);
        this.server.emit(CoreEvents.CHANGE_PASSWORD, client.login);
    }

    @OnEvent(CoreEvents.ERROR_AUTH)
    handleERROR_AUTH(client: Client, error: string) {
        this.logger.error(`${client.login} ERROR_AUTH`);
        this.server.emit(CoreEvents.ERROR_AUTH, client.login, error);
    }

    @OnEvent(CoreEvents.ERROR_FILL_FORM)
    handleERROR_FILL_FORM(client: Client) {
        this.logger.error(`${client.login} ERROR_FILL_FORM`);
        this.server.emit(CoreEvents.ERROR_FILL_FORM, client.login);
    }

    @OnEvent(CoreEvents.AVAILABLE_DATES, { async: true })
    async handleAVAILABLE_DATES(client: Client, dates: IDates[], type: 'search' | 'cron') {
        const queue = 'https://message-queue.api.cloud.yandex.net/b1g6ebo1n8i27mbfffi6/dj6000000000g2gr02t8/sign.fifo';
        const ApproximateNumberOfMessages = +await this.sqs.getInfo(queue);

        if (type == 'search' && ApproximateNumberOfMessages == 0) {
            const [res] = await this.gs.getClients({ ...client.attempts }, client.tab, 5000);

            const clients = res.filter(({ afterDate, beforeDate }) => dates.some(date => date.date >= afterDate && date.date <= beforeDate));

            // TODO: Продумать процесс добавления клиетов в очередь
            this.sqs.sendMessage(queue, _.chunk(clients.sort((a, b) => b.importance - a.importance), 5))
                .catch(err => console.log(err));

            const seconds = new Date().getSeconds();
            this.coreService.addCronJob('Запись', `${seconds + 2} */1 * * * *`, queue);
        }
        this.logger.log(`${client.login} AVAILABLE_DATES`);
        this.server.emit(CoreEvents.AVAILABLE_DATES, client.login, dates, type);
    }

    @OnEvent(CoreEvents.DATE_DOES_NOT_FIT)
    handleDATE_DOES_NOT_FIT(client: Client, value: string = 'Нет доступных мест для записи') {
        this.logger.log(`${client.login} DATE_DOES_NOT_FIT`);
        this.server.emit(CoreEvents.DATE_DOES_NOT_FIT, client.login, value);
    }

    @OnEvent(CoreEvents.SUCCESS_SIGNED_UP)
    handleSUCCESS_SIGNED_UP(client: Client) {
        this.logger.log(`${client.login} SUCCESS_SIGNED_UP`);
        this.server.emit(CoreEvents.SUCCESS_SIGNED_UP, client.login);
    }
}