import { Controller, Post, Body, Get } from '@nestjs/common';
import { CoreService } from './core.service';

@Controller('core')
export class CoreController {

    constructor(private coreService: CoreService) { }

    @Get('getCrons')
    getCrons() {
        return this.coreService.getCrons;
    }

    @Get('getListeners')
    getListeners() {
        return this.coreService.getListeners;
    }

    @Post('turnOn')
    start(
        @Body('queue') queue: string,
        @Body('startAt') startAt?: string
    ) {
        this.coreService.turnOn(queue, startAt);
        return 'OK';
    }

    @Post('turnOff')
    stop(
        @Body('queue') queue: string,
    ) {
        this.coreService.turnOff(queue);
        return 'OK';
    }

    @Post('cronOn')
    startCron(
        @Body('name') name: string,
        @Body('mask') mask: string,
        @Body('queue') queue: string,
    ) {
        this.coreService.addCronJob(name, mask, queue);
        return 'OK';
    }

    @Post('cronOff')
    stopCron(
        @Body('name') name: string,
    ) {
        this.coreService.deleteCron(name);
        return 'OK';
    }
}
