export enum URLs {
    MAIN = "https://cgifederal.secure.force.com",
    APPLICANTHOME = "https://cgifederal.secure.force.com/applicanthome",
    ERROR_AUTHENTICATION = "https://cgifederal.secure.force.com/SiteLogin",
    CHANGE_PASSWORD = "https://cgifederal.secure.force.com/_ui/system/security/ChangePassword",
    SCHEDULE_APPOINTMENT = "https://cgifederal.secure.force.com/scheduleappointment",
    APPOINTMENT_CONFIRMATION = "https://cgifederal.secure.force.com/appointmentconfirmation"
}

export enum PathName {
    MAIN = '/',
    APPLICANTHOME = '/applicanthome',
    ERROR_AUTHENTICATION = '/SiteLogin',
    CHANGE_PASSWORD = '/_ui/system/security/ChangePassword',
    SCHEDULE_APPOINTMENT = '/scheduleappointment',
    APPOINTMENT_CONFIRMATION = '/appointmentconfirmation',
}

export enum AuthStatus {
    SUCCESS_AUTH,
    ERROR_AUTHENTICATION,
    CHANGE_PASSWORD,
    UNKNOWN_ERROR
}

export enum SignStatus {
    SUCCESS_SIGN_UP,
    TRY,
    WHITE_WALL,
    DATE_DOES_NOT_FIT,
    UNKNOWN_ERROR
}

export enum Town {
    MOSCOW = 'MSK',
    YEKATERINBURG = 'EKB',
    VLADIVOSTOK = 'VLD',
    DROPBOX = 'DRB',
    KAZ = 'KAZ',
    QUICK = 'QUICK'
}

export enum SearchFlow {
    ERROR_AUTHENTICATION,
    CHANGE_PASSWORD,
    NO_DATES_AVAILABLE,
    DATES_AVAILABLE,
    UNKNOWN_ERROR
}

export enum ErrorCore {
    ERROR_AUTHORIZATION,
    ERROR_FILL_FORM,
    ERROR_TIME_TABLE,
    ERROR_SIGN
}