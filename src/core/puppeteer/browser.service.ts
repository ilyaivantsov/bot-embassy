import { Injectable } from '@nestjs/common';
import { join } from 'path';
import puppeteer from "puppeteer";
import Puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import UserAgentOverride from 'puppeteer-extra-plugin-stealth/evasions/user-agent-override';
import { CaptchaService } from '../captcha/captcha.service';
import { ITimeTableStatus, IDates, ISignUpStatus, ISuitableDates, IAuthStatus } from '../interfaces';
import { AuthStatus, URLs } from '../enum/core.enum';

declare module 'puppeteer' {
    export interface Page {
        waitForTimeout(duration: number): Promise<void>;
    }
}

@Injectable()
export class BrowserService {

    constructor(private captchaService: CaptchaService) {
        const stealth = StealthPlugin();
        stealth.enabledEvasions.delete('user-agent-override');
        Puppeteer.use(stealth);
        const ua = UserAgentOverride({
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
            locale: 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            platform: 'Win64'
        });
        Puppeteer.use(ua);
    }

    async getBrowserInstance(launchOptions: puppeteer.LaunchOptions, dirname: string = 'default'): Promise<puppeteer.Browser> {
        const browser: puppeteer.Browser = await Puppeteer.launch({
            ...launchOptions,
            userDataDir: join(__dirname, '..', '..', '..', '..', 'chrome', dirname)
        });
        return browser;
    }

    async getPageInstance(browser: puppeteer.Browser, credentials: puppeteer.AuthOptions = null): Promise<puppeteer.Page> {
        const page = await browser.newPage();
        await page.authenticate(credentials);
        return page;
    }

    async authorization(page: puppeteer.Page, login: string, password: string): Promise<puppeteer.Page> {
        await page.goto(URLs.MAIN);
        await page.waitForSelector('img[xmlns]');
        const [Login, capcha] = await page.$$('input[type="text"]');
        const Password = await page.$('input[type="password"]');
        const checkbox = await page.$('input[type="checkbox"]');
        const base64ImgCaptcha: string = await page.evaluate(() => document.querySelector('img').src);
        await checkbox.click();

        await Login.type(login);
        await Password.type(password);
        await capcha.type(await this.captchaService.solver(base64ImgCaptcha));

        await Promise.all([
            page.waitForNavigation(),
            page.click('input[type="submit"]'),
        ]);

        await page.waitForTimeout(6000);

        return page;
    }

    async authorizationStatus(page: puppeteer.Page): Promise<IAuthStatus> {
        let URL: string = await this.getCurrentPageURL(page);
        switch (URL) {
            case URLs.APPLICANTHOME:
                return { page, status: AuthStatus.SUCCESS_AUTH };
            case URLs.CHANGE_PASSWORD:
                return { page, status: AuthStatus.CHANGE_PASSWORD };
            case URLs.ERROR_AUTHENTICATION:
                return { page, status: AuthStatus.ERROR_AUTHENTICATION };
            default:
                return { page, status: AuthStatus.UNKNOWN_ERROR };
        }
    }

    async getCurrentPageURL(page: puppeteer.Page): Promise<string> {
        let URL: string = page.url().split('?')[0];
        return URL;
    }

    async getPathNameOfCurrentPage(page: puppeteer.Page): Promise<string> {
        return await page.evaluate(() => {
            return location.pathname;
        });
    }

    async toFillOutFormsMsk(page: puppeteer.Page): Promise<puppeteer.Page> {
        let nextSteps: puppeteer.ElementHandle<Element> | puppeteer.ElementHandle<Element>[],
            ansArr: puppeteer.ElementHandle<Element>[];
        // Step 1.
        const buttonApply = await page.$('.current');
        await buttonApply.click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 2.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[0].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 3.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[0].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 4.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[2].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 5.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[1].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 6.
        nextSteps = await page.$('input[type="button"]');
        await nextSteps.click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 7.
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 8.
        nextSteps = await page.$$('input[type="submit"]'); // No. Belrus
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 9. 
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('button');
        // Step 10.
        const submitInfoFromModalWindow = await page.$('button');
        await submitInfoFromModalWindow.click();
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 11. Calendar
        return page;
    }

    async toFillOutFormsEkat(page: puppeteer.Page): Promise<puppeteer.Page> {
        let nextSteps: puppeteer.ElementHandle<Element> | puppeteer.ElementHandle<Element>[],
            ansArr: puppeteer.ElementHandle<Element>[];
        // Step 1.
        const buttonApply = await page.$('.current');
        await buttonApply.click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 2.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[0].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 3.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[4].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 4.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[1].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 5.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[1].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 6.
        nextSteps = await page.$('input[type="button"]');
        await nextSteps.click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 7.
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 8.
        nextSteps = await page.$$('input[type="submit"]'); // No. Belrus
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 9. Disabled
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[2].click();
        await page.waitForNavigation();
        await page.waitForSelector('input[type="submit"]');
        // Step 9. 
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('button');
        // Step 10.
        const submitInfoFromModalWindow = await page.$('button');
        await submitInfoFromModalWindow.click();
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 11. Calendar
        return page;
    }

    async toFillOutFormsVld(page: puppeteer.Page): Promise<puppeteer.Page> {
        let nextSteps: puppeteer.ElementHandle<Element> | puppeteer.ElementHandle<Element>[],
            ansArr: puppeteer.ElementHandle<Element>[];
        // Step 1.
        const buttonApply = await page.$('.current');
        await buttonApply.click();
        await page.waitForNavigation();
        // Step 2.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[0].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 3.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[3].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 4.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[1].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 5.
        nextSteps = await page.$$('input[type="submit"]');
        ansArr = await page.$$('input[type="radio"]');
        await ansArr[1].click();
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 6.
        nextSteps = await page.$('input[type="button"]');
        await nextSteps.click();
        await page.waitForNavigation();
        // Step 7.
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 8.
        nextSteps = await page.$$('input[type="submit"]'); // No. Belrus
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 9. 
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        await page.waitForSelector('button'); // Wait for modal window
        // Step 10.
        const submitInfoFromModalWindow = await page.$('button');
        await submitInfoFromModalWindow.click();
        nextSteps = await page.$$('input[type="submit"]');
        await nextSteps[1].click();
        await page.waitForNavigation();
        // Step 11. Calendar
        return page;
    }

    async checkTimetable(page: puppeteer.Page, duration: number, preload: boolean = false): Promise<ITimeTableStatus> {
        if (preload) {
            await page.goto(URLs.SCHEDULE_APPOINTMENT);
        }
        const timeTable: puppeteer.ElementHandle<Element>[] = await page.$$('td[onclick]');

        if (!timeTable.length) {
            await page.waitForTimeout(duration);
            await page.reload();
            return { page, dates: [] };
        }

        let dates = await this.getDates(page);
        return { page, dates };

    }

    async toSignUp(page: puppeteer.Page, suitableDates: ISuitableDates, preload: boolean = false): Promise<ISignUpStatus> {
        if (preload) {
            await page.goto(URLs.SCHEDULE_APPOINTMENT);
        }

        const timeTable: puppeteer.ElementHandle<Element>[] = await page.$$('td[onclick]');

        if (!timeTable.length) {
            return { page, signed: false };
        }

        let dates: IDates[] = await this.getDates(page);
        let indexs: IDates[] = dates.filter(date => date.date >= suitableDates.after && date.date <= suitableDates.before);

        if (!indexs.length) {
            return { page, signed: false };
        }

        if (timeTable.length > 1) {
            await timeTable[indexs[Math.floor(Math.random() * indexs.length)].index].click();
            await page.waitForTimeout(3000);
        }

        const selectDate = await page.$$('input[type="checkbox"]');
        await selectDate[Math.floor(Math.random() * selectDate.length)].click();
        const submit = await page.$('input[type="button"]');
        // await submit.click();
        await Promise.all([
            page.waitForNavigation({ waitUntil: 'domcontentloaded' }),
            submit.click(),
        ]);
        return { page, signed: true };

    }

    async getDates(page: puppeteer.Page): Promise<IDates[]> {
        let time: IDates[] = await page.evaluate(() => {
            const currYear = new Date().getFullYear();

            let dates: IDates[] = [],
                currentMonth = new Date().getMonth(),
                timeTable = document.querySelectorAll('td[onclick]');
            timeTable.forEach((elm, index) => {
                var month = elm.closest('.ui-datepicker-group').querySelector('.ui-datepicker-month').textContent.toLowerCase();
                var m = _getNumOfMonth(month),
                    d = +elm.textContent,
                    y = currentMonth > m ? currYear + 1 : currYear;

                dates.push({ date: new Date(y, m, d).getTime(), index });
            });
            function _getNumOfMonth(mon) {
                let s: number;
                switch (mon) {
                    case "январь": s = 0; break;
                    case "февраль": s = 1; break;
                    case "март": s = 2; break;
                    case "апрель": s = 3; break;
                    case "май": s = 4; break;
                    case "июнь": s = 5; break;
                    case "июль": s = 6; break;
                    case "август": s = 7; break;
                    case "сентябрь": s = 8; break;
                    case "октябрь": s = 9; break;
                    case "ноябрь": s = 10; break;
                    case "декабрь": s = 11; break;
                }
                return s;
            }
            return dates;
        });
        return time;
    }

    async screenshot(page: puppeteer.Page, prefix: string = 'default') {
        await page.setViewport({ width: 1024, height: 800 });
        await page.screenshot({
            path: join(__dirname, '..', '..', '..', '..', 'screen', `${prefix}_${Date.now()}.jpeg`),
            type: "jpeg",
            fullPage: true
        });
    }
}
