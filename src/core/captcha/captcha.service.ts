import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Anticaptcha from './anticaptcha';

@Injectable()
export class CaptchaService {

    constructor(private configService: ConfigService) { }

    private readonly anticaptcha = Anticaptcha(this.configService.get<string>('ANTICAPTCHA_API'));

    async solver(base64Img: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.anticaptcha.getBalance((err, balance) => {
                if (err) {
                    reject({ msg: 'Low money', type: 64 });
                }

                this.anticaptcha.setMinLength(5);

                if (balance > 0) {
                    this.anticaptcha.createImageToTextTask({
                        case: true, // or params can be set for every captcha specially
                        body: base64Img.slice(18) // cut data:image;base64, info
                    },
                        (err, taskId) => {
                            if (err) {
                                reject({ msg: 'Error Id captcha', type: 64 });
                            }

                            this.anticaptcha.getTaskSolution(taskId, (err, taskSolution) => {
                                if (err) {
                                    reject({ msg: 'Error Task captcha', type: 64 });
                                }

                                resolve(taskSolution);
                            });
                        });
                }
            });
        });


    }

}